using System;
using QwackBot;
using QwackBot.Modules;
using Xunit;

namespace QwackBotTest
{
    public class MiscTest
    {
        [Fact]
        public void CanGetWikiInfo()
        {
            string s = new Wiki().Scrap();
            Assert.DoesNotContain("[[", s);
            Assert.DoesNotContain("]]", s);
            Assert.DoesNotContain("File", s);
            Assert.DoesNotContain("<ref", s);
            Assert.DoesNotContain("{{", s);
            Assert.DoesNotContain("}}", s);
            Assert.DoesNotContain("* ", s);
            Assert.DoesNotContain("http", s);
            Assert.DoesNotContain("Category", s);
            Assert.DoesNotContain("\n\n\n", s);
        }

        [Fact]
        public void TruncateMessageEvery1999Chars()
        {
            string bigMessage = "";
            for (int i = 0; i < 4000; i++)
            {
                bigMessage += "a";
            }

            var truncateMessage = Misc.TruncateMessage(bigMessage);
            Assert.Equal(truncateMessage.Count, 4000/1999+1);
        }

        [Fact]
        public void TruncateMessageEvery1999CharsEvenIfLengthMessageIsLessThan1999()
        {
            string message = "";
            for (int i = 0; i < 10; i++)
            {
                message += "a";
            }

            var truncateMessage = Misc.TruncateMessage(message);
            Assert.Single(truncateMessage);
        }
    }
}
