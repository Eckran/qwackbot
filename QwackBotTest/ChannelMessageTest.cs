using System;
using System.Reflection;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Moq;
using QwackBot;
using QwackBot.Modules;
using Xunit;

namespace QwackBotTest
{    
    public class ChannelMessageTest
    {
        
        public void TestMethod1()
        {
            var client = new DiscordSocketClient();
            long uid = 1001;

            var channel = new Mock<SocketTextChannel>(client, uid, new Mock<SocketGuild>());

            var socketUser = new Mock<SocketEntity<ulong>>(client, uid);

            ConstructorInfo[] constructors =
                typeof(SocketUserMessage).GetConstructors(
                    BindingFlags.NonPublic | BindingFlags.Instance);
            SocketUserMessage msg = (SocketUserMessage)constructors[0]
                .Invoke(null, new object[] { client, uid, channel, socketUser, new MessageSource() });

            //var msg = CreateInstance<SocketUserMessage>(client, uid, channel, socketUser, new MessageSource());

            var context = new SocketCommandContext(client, msg);
            var mockContext = new Mock<Misc>();
            mockContext.Setup(misc => misc.Context).Returns(context);

            mockContext.Object.Ping().GetAwaiter().GetResult();
        }

        public static T CreateInstance<T>(params object[] args)
        {
            var type = typeof(T);
            var instance = type.Assembly.CreateInstance(
                type.FullName, false,
                BindingFlags.Instance | BindingFlags.NonPublic,
                null, args, null, null);
            return (T)instance;
        }
    }
}
