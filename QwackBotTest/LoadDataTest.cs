﻿using System;
using System.IO;
using QwackBot;
using QwackBotTest;
using Xunit;

namespace QwackBotTest
{
    public class LoadDataTest
    {
        [Fact]
        public void ExitIfFileIsMissing()
        {
            IOException ex = Assert.Throws<IOException>(() => new LoadData("coincoin.json"));

            Assert.Equal("File not found", ex.Message);
        }

        [Fact]
        public void ExitIfKeyInvalid()
        {
            IOException ex = Assert.Throws<IOException>(() => new LoadData("dataBad.json"));

            Assert.Equal("Key not found", ex.Message);                    
        }

    }
}
