﻿using System;
using QwackBot;
using QwackBot.Models;
using System.Reflection;
using Tweetinvi.Core.Extensions;
using Xunit;
using Xunit.Sdk;

namespace QwackBotTest
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class TestBeforeAfter : BeforeAfterTestAttribute
    {

        public override void Before(MethodInfo methodUnderTest)
        {
            using (var db = new BotDbContext())
            {
                db.BadWord.Add(new BadWord() { insult = "putain" });
                db.SaveChanges();
            }
        }

        public override void After(MethodInfo methodUnderTest)
        {
            using (var db = new BotDbContext())
            {
                db.BadWord.ForEach(b => db.Remove(b));
                db.SaveChanges();
            }
        }
    }
    public class CommandHandlerTest
    {
        [Fact]
        public void DoNotSuppressALetter()
        {
            CommandeHandler commandeHandler = new CommandeHandler();
            bool isInsultDetected = commandeHandler.IsInsultDetected("a");
            Assert.False(isInsultDetected);
        }

        [Fact]
        public void DoNotConsiderALinkAsABadword()
        {           
            CommandeHandler commandeHandler = new CommandeHandler();
            bool isInsultDetected = commandeHandler.IsInsultDetected("https://vignette.wikia.nocookie.net/goanimate-v3/images/b/b6/150px-Misty_SM.png/revision/latest?cb=20180901204619");
            Assert.False(isInsultDetected);
        }

        [Fact]
        [TestBeforeAfter]
        public void CanDetectABadWord()
        {
            using (var db = new BotDbContext())
            {
                foreach (var badWord in db.BadWord)
                {
                    Console.WriteLine(badWord.insult);
                }
            }

            CommandeHandler commandeHandler = new CommandeHandler();
            bool isInsultDetected = commandeHandler.IsInsultDetected("putain");
            Assert.True(isInsultDetected);
        }

        [Fact]
        [TestBeforeAfter]
        public void CanDetectABadWordInSentence()
        {
            CommandeHandler commandeHandler = new CommandeHandler();
            bool isInsultDetected = commandeHandler.IsInsultDetected("putain de merde !");
            Assert.True(isInsultDetected);
        }
    }
}
