﻿using System;
using System.IO;
using Ical.Net.DataTypes;
using QwackBot;
using QwackBotTest;
using Xunit;

namespace QwackBotTest
{
    public class PlanningTest
    {
        [Fact]
        public void CanDownLoadFile()
        {
            Assert.NotEmpty(new Planning().Get());
        }

        [Fact]
        public void canCompareDates()
        {
            Assert.Equal(-1, DateTime.Compare(new CalDateTime("20181115T080000Z").Date, DateTime.Now ));            
        }        
    }
}
