﻿using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;
using QwackBot.Models;

namespace QwackBot
{
    class Program
    {
        private DiscordSocketClient client;
        private CommandeHandler commandeHandler;
        private BotDbContext db;

        static void Main(string[] args)
        {
           new Program().Start().GetAwaiter().GetResult();
        }

        private async Task Start()
        {
            db = new BotDbContext();
            db.Database.EnsureCreated();
            FillDbIfEmpty();

            if (isTokenNullOrEmpty()) return;
            client = new DiscordSocketClient(new DiscordSocketConfig
            {
                LogLevel = LogSeverity.Verbose
            });
            client.Log +=onClientLog;
            client.ChannelCreated += ClientOnChannelCreated;
            client.UserJoined += ClientOnUserJoined;
            client.GuildAvailable += ClientOnGuildAvailable;
            await client.LoginAsync(Config.bot.type, Config.bot.token);
            await client.StartAsync();
            commandeHandler = new CommandeHandler();
            await commandeHandler.InitializeAsync(client);
            await Task.Delay(-1);
        }

        private async Task ClientOnGuildAvailable(SocketGuild socketGuild)
        {
            foreach (var socketGuildTextChannel in socketGuild.TextChannels)
            {
                Console.WriteLine($"Connected to chan: {socketGuildTextChannel.Name}");
            }
        }

        private async Task ClientOnUserJoined(SocketGuildUser socketGuildUser)
        {
            Console.WriteLine($"{socketGuildUser.Username} {socketGuildUser.Id} has joined");
            User user = new Repository.User(db).Get(socketGuildUser.Id);
            if (user == null)
            {                
                user = new Repository.User(db).Add(User.Create(socketGuildUser.Id));
                Console.WriteLine($"Create new user {socketGuildUser.Username}");
            }            
           
            await socketGuildUser.SendMessageAsync($"Coin coin {socketGuildUser.Username} tu as {user.Points} points, attention au ban automatique ;)");
        }

        private async Task ClientOnChannelCreated(SocketChannel socket)
        {
            var socketTextChannel = socket as SocketTextChannel;
            await socketTextChannel.SendMessageAsync("Coin Coin mes canards");
        }

        private bool isTokenNullOrEmpty()
        {
            if (string.IsNullOrEmpty(Config.bot.token))
            {
                Console.WriteLine("Bot token is null");
                Console.WriteLine("Stopping...");
                return true;
            }

            return false;
        }

        private Task onClientLog(LogMessage msg)
        {
            Console.WriteLine(msg);
            return Task.CompletedTask;
        }

        private void FillDbIfEmpty()
        {
            String path = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), @"data.json");
            LoadData loadData = new LoadData(path);

            if (new Repository.BadWord(db).Count() > 0)
            {
                return;
            }

            int nb = new Repository.BadWord(db).AddAll(loadData.data.BadWords);
            nb += new Repository.DuckJoke(db).AddAll(loadData.data.DuckJokes);
            nb += new Repository.ImagePorn(db).AddAll(loadData.data.ImagePorn);
            nb += new Repository.Recipe(db).AddAll(loadData.data.DuckRecipes);
            nb += new Repository.Anecdote(db).AddAll(loadData.data.Anecdotes);
            Console.WriteLine($"New entries: {nb}");
        }
    }
}
