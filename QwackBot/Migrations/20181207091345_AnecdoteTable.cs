﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QwackBot.Migrations
{
    public partial class AnecdoteTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Anecdote",
                columns: table => new
                {
                    AnecdoteId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    anecdote = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Anecdote", x => x.AnecdoteId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Anecdote");

        }
    }
}
