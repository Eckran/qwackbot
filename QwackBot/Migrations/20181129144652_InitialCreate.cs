﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace QwackBot.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BadWord",
                columns: table => new
                {
                    BadWordId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    insult = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BadWord", x => x.BadWordId);
                });

            migrationBuilder.CreateTable(
                name: "DuckJoke",
                columns: table => new
                {
                    DuckJokeId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    joke = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DuckJoke", x => x.DuckJokeId);
                });

            migrationBuilder.CreateTable(
                name: "ImagePorn",
                columns: table => new
                {
                    ImagePornId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    uri = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ImagePorn", x => x.ImagePornId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BadWord");

            migrationBuilder.DropTable(
                name: "DuckJoke");

            migrationBuilder.DropTable(
                name: "ImagePorn");
        }
    }
}
