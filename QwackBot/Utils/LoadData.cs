﻿using Newtonsoft.Json;
using QwackBot.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace QwackBot
{
    public class LoadData
    {
        public class JsonData
        {
            public List<string> BadWords;
            public List<string> ImagePorn;
            public List<string> DuckJokes;
            public List<string> DuckRecipes;
            public List<string> Anecdotes;
            public List<Quizz> Quizz;
        }

        private JsonData _data;

        public JsonData data { get => _data; set => _data = value; }

        public LoadData(string jsonFile)
        {
            if (File.Exists(jsonFile))
            {
                JsonData deserializeData = DeserializeData(jsonFile);

                if (!AllKeysArePresent(deserializeData))
                { 
                    throw new IOException("Key not found");
                }

                data = deserializeData;
            }
            else
            {
                throw new IOException("File not found");
            }            
        }

        private JsonData DeserializeData(string file)
        {
            var jsonDataText = File.ReadAllText(file);
            return JsonConvert.DeserializeObject<JsonData>(jsonDataText);            
        }

        private bool AllKeysArePresent(JsonData data)
        {
            return data?.BadWords != null && data.BadWords.Count != 0 && data.DuckJokes != null && data.DuckJokes.Count != 0 && data.Quizz != null && data.Quizz.Count != 0 && data.ImagePorn != null && data.ImagePorn.Count != 0 && data.DuckRecipes != null && data.DuckRecipes.Count != 0 && data.Anecdotes != null && data.Anecdotes.Count != 0;
        }
    }
}
