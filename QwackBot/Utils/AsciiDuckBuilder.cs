﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QwackBot
{
    class AsciiDuckBuilder
    {
        private string finalMessage = "";

        private string duckIsComing = "DUCK IS COMING";
        private string coin = "Coin Coin\n";

        private string leftDuck =   "           "   + "\n" +
                                    @"    \_\_"     + "\n" +
                                    @"<(o )\_\_\_"  + "\n" +
                                    @"   (  .\_> /" + "\n" +
                                    @"     \`---' " + "\n";

        private string rightDuck =  "           "   + "\n" +
                                    @"        \_\_" + "\n" +
                                    @"\_\_\_( o)>"  + "\n" +
                                    @"\ < \_. )"    + "\n" +
                                    @"   \`---'"    + "\n";

        public string FwackMessage()
        {
            finalMessage = duckIsComing;
            for (int i = 0; i < 10; i++)
            {
                finalMessage = finalMessage + leftDuck + rightDuck + coin;
            }
            return finalMessage;
        }

        public string FwackMessage(int duckPopulation)
        {
            finalMessage = duckIsComing;
            for (int i = 0; i < duckPopulation; i++)
            {
                finalMessage = finalMessage + leftDuck + rightDuck + coin;
            }
            return finalMessage;
        }
    }
}
