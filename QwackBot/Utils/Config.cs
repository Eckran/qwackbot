﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Discord;
using Newtonsoft.Json;
using QwackBot.Models;

namespace QwackBot
{
    public static class Config
    {
        public static Bot bot { get; private set; } = new Bot() { cmdPrefix = "!", token = Environment.GetEnvironmentVariable("qwackBotToken"), type = TokenType.Bot };
        public static readonly Random random = new Random();


        public struct Bot
        {
            public string token;
            public string cmdPrefix;
            public TokenType type;
        }

    }
}