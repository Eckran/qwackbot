﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using QwackBot.Models;
using TwitterBot;


namespace QwackBot.Modules
{
    public class Misc: ModuleBase<SocketCommandContext>
    {
        private BotDbContext db = new BotDbContext();

        [Command("ping")]
        public async Task Ping()
        {
            await Context.Channel.SendMessageAsync("Pong !");
        }

        [Command("canard")]
        public async Task Canard()
        {
            await Context.Channel.SendMessageAsync("Coin Coin!");
        }

        [Command("twit")]
        public async Task MpTweetToken()
        {            
            var dmChannel = await Context.User.GetOrCreateDMChannelAsync();
            await dmChannel.SendMessageAsync(
                "Merci de me donner le token d'accès au compte twitter. Pour en créer un, il vous faut un compte développeur.\n" +
                "Il faut que le message soit formatté comme ci dessous, sans esapce en une seule fois avec ',' comme séparateur:\n" +
                "!token consumerKey,consumerSecret,accessToken,accessTokenSecret");            
        }

        [Command("token")]        
        public async Task Tweeter([Remainder]string tokens)
        {
            var dmChannel = await Context.User.GetOrCreateDMChannelAsync();
            string[] tokenList = tokens.Split(",");
            if (tokenList.Length != 4)
            {
                await dmChannel.SendMessageAsync("Y a qwouille dans l'étang, t'as pas tout bien noté mon canard ;)");
                return;
            }

            dmChannel.SendMessageAsync("J'ai lancé ça, vérifie ton compte, bisous :)");

            new Twitter(tokenList[0], tokenList[1], tokenList[2], tokenList[3]).RetweetGameWithPattern(Twitter.GamePatternFr[0]);                        
        }

        [Command("image")]
        public async Task DuckPorn()
        {            
            await Context.Channel.SendMessageAsync(new Repository.ImagePorn(db).GetRandom());           
        }

        [Command("recette")]
        public async Task Recette()
        {            
            await Context.Channel.SendMessageAsync($"Petite recette : {new Repository.Recipe(db).GetRandom()}");
        }

        [Command("anecdote")]
        public async Task Anecdote()
        {
            await Context.Channel.SendMessageAsync(new Repository.Anecdote(db).GetRandom());        
        }

        [Command("qwackboursier")]
        public async Task QwackBoursier()
        {
            await Context.Channel.SendMessageAsync($"C'est la Qwuise: {new Bitcoin().GetBitCoinValue()} €");
        }

        [Command("blague")]
        public async Task DuckJoke()
        {
            await Context.Channel.SendMessageAsync(new Repository.DuckJoke(db).GetRandom());
        }


        [Command("info")]
        public async Task DuckInfo()
        {
            var wikiInfo = new Wiki().Scrap();
            List<string> msg = TruncateMessage(wikiInfo);

            foreach (var s in msg)
            {
                await Context.Channel.SendMessageAsync(s);
            }
        }

        [Command("planning")]
        public async Task Planning()
        {
            var summary = new Planning().Get();
            List<string> msg = TruncateMessage(summary);

            foreach (var s in msg)
            {
                await Context.Channel.SendMessageAsync(s);
            }
        }

        public static List<string> TruncateMessage(string value)
        {
            int size = 1999;
            List<string> triplets = new List<string>();
            for (int i = 0; i < value.Length; i += size)
            {
                triplets.Add(i + size > value.Length ? value.Substring(i) : value.Substring(i, size));
            }

            return triplets;
        }

        [Command("fwack")]
        public async Task Fwack([Remainder]int duckPopulation)
        {
            string finalMessage = "";

            if (duckPopulation < 5)
            {
                duckPopulation = 5;
            }
            else if (duckPopulation > 15)
            {
                duckPopulation = 15;
            }
            

            AsciiDuckBuilder asciiDuckBuilder = new AsciiDuckBuilder();

            finalMessage = asciiDuckBuilder.FwackMessage(duckPopulation);

            await Context.Channel.SendMessageAsync(finalMessage);
        }

        [Command("fwack")]
        public async Task Fwack()
        {
            string finalMessage = "";

            AsciiDuckBuilder asciiDuckBuilder = new AsciiDuckBuilder();

            finalMessage = asciiDuckBuilder.FwackMessage();

            await Context.Channel.SendMessageAsync(finalMessage);
        }       

        [Command("points")]
        public async Task GetUserPoints()
        {
            float points = new Repository.User(db).GetPoints(Context.User.Id);
            if (points > 0F)
            {
                await Context.Channel.SendMessageAsync($"Ho le suceur ! {Context.User.Username} a {points} !");
            }
            else
            {
                await Context.Channel.SendMessageAsync($"Il vit dangereusement ! {Context.User.Username} a {points} !");
            }
        }

        [Command("help")]
        public async Task Help()
        {
            Dictionary<string, string> listCommand = new Dictionary<string, string>();

            listCommand.Add("!anecdote", "Decouvrez une anecdote sur nos amis les canards !");
            listCommand.Add("!blague", "Rigolez avec une blague sur les canards !");
            listCommand.Add("!canard", "Salutations de canard.");
            listCommand.Add("!fwack", "Une armée de canards pour rester à l'abri des regards.");
            listCommand.Add("!help", "Liste de toutes les commandes disponible du QwackBot.");
            listCommand.Add("!image", "Quelques images de canards.");
            listCommand.Add("!info", "Des informations sur les canards.");
            listCommand.Add("!ping", "Pong !");
            listCommand.Add("!quizz", "Petit quizz sur les canards.");
            listCommand.Add("!qwackboursier", "Renseignement sur le cours du BitCoin.");
            listCommand.Add("!recette", "Quelques recette à base de canard.");
            listCommand.Add("!token", "Inscription sur des jeux concours Twitter.");
            listCommand.Add("!points", "Affiche le nombre de points de l'utilisateur.");
            //listCommand.Add("", "");

            string finalMessage = "\n";

            foreach (KeyValuePair<string, string> entry in listCommand)
            {
                finalMessage += entry.Key + " -> " + entry.Value + "\n\n";
            }

            await Context.Channel.SendMessageAsync(finalMessage);
        }
    }
}
