﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using System.Reflection;
using Discord;
using QwackBot.Models;
using System.Threading;

namespace QwackBot
{
    public class CommandeHandler
    {
        private DiscordSocketClient client;
        private CommandService service;
        private BotDbContext db;

        public static QuizzSession quizzSession = new QuizzSession();

        public static Question question = new Question();

        private bool isQuizzInProgress;

        public CommandeHandler()
        {
            db = new BotDbContext();
        }

        public async Task InitializeAsync(DiscordSocketClient client)
        {
            this.client = client;
            service = new CommandService();            
            await service.AddModulesAsync(Assembly.GetEntryAssembly());
            client.MessageReceived += OnMessageReceived;
        }

        private async Task OnMessageReceived(SocketMessage socketMessage)
        {
            var socketUserMessage = socketMessage as SocketUserMessage;
            if (socketUserMessage == null) { return; }
            Console.WriteLine(socketUserMessage.Author.Username);
            CreateUserIfNeeded(socketUserMessage);
            if (IsInsultDetected(socketUserMessage.Content))
            {
                Console.WriteLine($"insult: {socketUserMessage.Author.Username}");
                await socketUserMessage.DeleteAsync();
                await socketUserMessage.Channel.SendMessageAsync("Bad ducky do not not say that! >:(");
                bool isBanable = RemovePoint(socketUserMessage);
                if (isBanable) await BanUser(socketUserMessage);
                return;
            }

            ManageUser(socketUserMessage);
            await CheckForCommand(socketUserMessage);

            if (isQuizzInProgress && socketUserMessage.Author.Username != "Qwack Bot")
            {
                question.CheckAnswer(socketUserMessage.Author.Username, socketUserMessage.Content, question, quizzSession);
            }
        }

        private async Task BanUser(SocketUserMessage socketUserMessage)
        {            
            var socketGuildChannel = socketUserMessage.Channel as SocketGuildChannel;
            await socketGuildChannel.Guild.AddBanAsync(socketUserMessage.Author);
        }

        /**
        * Return if points is negative
        */
        private bool RemovePoint(SocketUserMessage socketUserMessage)
        {
            float points = new Repository.User(db).RemovePoint(socketUserMessage.Author.Id);            
            Console.WriteLine($"Points -- {points} for user {socketUserMessage.Author.Username}");
            return points < 0F;
        }

        private void ManageUser(SocketUserMessage socketUserMessage)
        {
            int nbMessages = new Repository.User(db).IncrementNbMessage(socketUserMessage.Author.Id);
            string points = null;
            if (socketUserMessage.Content.Length > 10)
            {
                points = new Repository.User(db).AddPoint(socketUserMessage.Author.Id).ToString();
            }
            if (points == null)
            {
                points = new Repository.User(db).GetPoints(socketUserMessage.Author.Id).ToString();
            }
            Console.WriteLine($"Message ++ {nbMessages} for points {points} for user {socketUserMessage.Author.Username} {socketUserMessage.Author.Id}");
        }

        private void CreateUserIfNeeded(SocketUserMessage socketUserMessage)
        {
            if (!new Repository.User(db).Exists(socketUserMessage.Author.Id))
            {                
                new Repository.User(db).Add(User.Create(socketUserMessage.Author.Id));
                Console.WriteLine($"Create new user {socketUserMessage.Author.Username}");
            }                
        }

        public bool IsInsultDetected(string content)
        {
            return new Repository.BadWord(db).ContainsBadWord(content);
        }

        private async Task CheckForCommand(SocketUserMessage socketUserMessage)
        {
            var socketCommandContext = new SocketCommandContext(client, socketUserMessage);
            int argPos = 0;
            if (socketUserMessage.HasStringPrefix(Config.bot.cmdPrefix, ref argPos) ||
                socketUserMessage.HasMentionPrefix(client.CurrentUser, ref argPos))
            {
                Console.WriteLine($"By {socketUserMessage.Author.Username}, command: {socketUserMessage.Content}");

                if (socketUserMessage.Content == "!quizz" && !isQuizzInProgress)
                {
                    Quizz(socketCommandContext);
                    isQuizzInProgress = true;
                }
                else if (socketUserMessage.Content == "!endquizz" && socketUserMessage.Author.Username == "Qwack Bot" && isQuizzInProgress)
                {
                    EndQuizz(socketCommandContext, quizzSession);
                    isQuizzInProgress = false;
                }

                var result = await service.ExecuteAsync(socketCommandContext, argPos);
                if (!result.IsSuccess && result.Error != CommandError.UnknownCommand)
                {
                    Console.WriteLine(result.ErrorReason);
                }
            }
        }

        [Command("quizz")]
        public async Task Quizz(SocketCommandContext socketCommandContext)
        {
            LoadData loadData = new LoadData("data.json");

            await socketCommandContext.Channel.SendMessageAsync("Un Quizz va commencer dans 10s. Tenez-vous prêt !");
            Thread.Sleep(10000);

            foreach (Question question in loadData.data.Quizz[0].Question)
            {
                // Ask question
                CommandeHandler.question = question;
                await socketCommandContext.Channel.SendMessageAsync(question.ImageUrl);
                await socketCommandContext.Channel.SendMessageAsync(question.QuestionString);
                Thread.Sleep(10000);
            }
            await socketCommandContext.Channel.SendMessageAsync("!endquizz");
        }

        [Command("endquizz")]
        public async Task EndQuizz(SocketCommandContext socketCommandContext, QuizzSession quizzSession)
        {
            LoadData loadData = new LoadData("data.json");

            quizzSession.ListQuizzPlayer.Sort((x, y) => x.Score.CompareTo(y.Score));

            if (quizzSession.ListQuizzPlayer.Last().Score == 0)
            {
                await socketCommandContext.Channel.SendMessageAsync("Fin du quizz. Aucun gagnant");
            }
            else
            {
                await socketCommandContext.Channel.SendMessageAsync("Fin du quizz. Le gagnant est : " + quizzSession.ListQuizzPlayer.Last().UserName + " avec " + quizzSession.ListQuizzPlayer.Last().Score + " points");
            }

            foreach (Question question in loadData.data.Quizz[0].Question)
            {
                question.CorrectAnswer = false;
            }

            quizzSession.ListQuizzPlayer = new List<Player>();
        }

    }
}
