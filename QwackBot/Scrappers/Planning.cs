﻿using System;
using System.Linq;
using Ical.Net;
using Ical.Net.CalendarComponents;
using Ical.Net.DataTypes;
using Newtonsoft.Json;
using Tweetinvi.Core.Extensions;

namespace QwackBot
{
    public class Planning
    {

        public string Get(){
            string icalText;

            using (var web = new System.Net.WebClient())
            {
                icalText = web.DownloadString("https://hp18.ynov.com/Telechargements/ical/Edt_LUVISON.ics?version=2018.0.3.3&idICal=EBBF6DA03081EE33C4EA58D496697D6C&param=643d5b312e2e36325d2666683d3126663d31");
            }

            var calendar = Calendar.Load(icalText);
            string final = "";
            foreach (CalendarEvent calendarEvent in calendar.Events)
            {                
                if (DateTime.Compare(calendarEvent.DtStart.Date, DateTime.Now) == 1
                && !String.IsNullOrEmpty(calendarEvent.Description))
                {
                    string @join = String.Join(" ", calendarEvent.Description.Split(' ').Distinct());
                    final += $"{@join}Du {calendarEvent.Start} au {calendarEvent.End} en {calendarEvent.Location}\n\n";
                }
            }

            return final;        
        }

    }
}
