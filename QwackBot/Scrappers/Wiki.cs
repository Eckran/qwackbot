﻿using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace QwackBot
{
    public class Wiki
    {

        public string Scrap(){
            string rawValue;

            using (var web = new System.Net.WebClient())
            {
                var url = "https://en.wikipedia.org/wiki/Duck?action=raw";
                rawValue = web.DownloadString(url);
                CleanTextFromRegex(ref rawValue, @"<ref.*>.*?<\/ref>");
                CleanTextFromRegex(ref rawValue, @"{{.*}}");
                CleanTextFromRegex(ref rawValue, @"{{((?:.*?\r?\n?)*)}}");
                CleanTextFromRegex(ref rawValue, @"\[\[File(.*?)(\]\].*)");
                CleanTextFromRegex(ref rawValue, @"\*\s.*");
                CleanTextFromRegex(ref rawValue, @"Category.*");                

                rawValue = rawValue.Replace("[[", "");
                rawValue = rawValue.Replace("]]", "");
                rawValue = rawValue.Replace("\n\n\n", "\n");                
                rawValue = rawValue.Replace("\n\n\n", "\n");                
            }

            if (rawValue == "")
            {
                return "La valeur n\'a pas pu etre trouvee.";
            }

            return rawValue.Trim();
        }

        private static void CleanTextFromRegex(ref string rawValue, string pattern)
        {
            string patternAutoGenerate = pattern;
            RegexOptions options = RegexOptions.Multiline;
            foreach (Match match in Regex.Matches(rawValue, patternAutoGenerate, options))
            {
                rawValue = rawValue.Replace(match.Value, "");
            }
        }
    }
}
