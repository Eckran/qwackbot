﻿using System;
using Newtonsoft.Json;

namespace QwackBot
{
    public class Bitcoin
    {

        public string GetBitCoinValue(){
            string json;

            using (var web = new System.Net.WebClient())
            {
                var url = "https://api.coindesk.com/v1/bpi/currentprice.json";
                json = web.DownloadString(url);
            }

            if (json == "")
            {
                return "La valeur n\'a pas pu etre trouvee.";
            }

            dynamic btcValue = JsonConvert.DeserializeObject(json);
            return btcValue.bpi.EUR.rate_float.Value.ToString();        
        }

    }
}
