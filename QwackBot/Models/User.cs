﻿using System;

namespace QwackBot.Models
{
    public class User
    {
        public int UserId { get; set; }
        public ulong DiscordId { get; set; }
        public float Points { get; set; }
        public string CreatedDate { get; set; }
        public int MessageNumber { get; set; }

        public static User Create(ulong discordId)
        {
            return new User { Points = 6, DiscordId = discordId, CreatedDate = new DateTime().ToLongDateString() };
        }
    }
}