﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace QwackBot.Models
{
    public class BotDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public DbSet<BadWord> BadWord { get; set; }
        public DbSet<ImagePorn> ImagePorn { get; set; }
        public DbSet<DuckJoke> DuckJoke { get; set; }
        public DbSet<Recipe> Recipe { get; set; }
        public DbSet<Anecdote> Anecdote { get; set; }
        public DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            String path = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), @"data.db");
            optionsBuilder.UseSqlite("Data Source="+ path);
        }
    }
}
