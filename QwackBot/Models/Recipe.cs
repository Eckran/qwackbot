﻿namespace QwackBot.Models
{
    public class Recipe
    {
        public int RecipeId { get; set; }
        public string recipe { get; set; }
    }
}
