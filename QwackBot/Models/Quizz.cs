﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QwackBot.Models
{
    public class Quizz
    {
        public string QuizzName;
        public List<Question> Question;
    }

    public class Question
    {
        public int QuestionNumber;
        public string ImageUrl;
        public string QuestionString;
        public string Answer;
        public bool CorrectAnswer;

        public void CheckAnswer(string userName, string userAnswer, Question question, QuizzSession quizzSession)
        {
            if (!quizzSession.CheckIfPlayerExist(userName, quizzSession))
            {
                quizzSession.ListQuizzPlayer.Add(new Player(userName, 0));
            }
            if (userAnswer == question.Answer && !question.CorrectAnswer)
            {
                Player p = quizzSession.ListQuizzPlayer.Find(x => x.UserName == userName);
                p.Score += 1;
                question.CorrectAnswer = true;
            }
        }
    }

    public class QuizzSession
    {
        public List<Player> ListQuizzPlayer { get; set; }

        public QuizzSession()
        {
            ListQuizzPlayer = new List<Player>();
        }
        public bool CheckIfPlayerExist(string userName, QuizzSession quizzSession)
        {
            foreach (Player p in quizzSession.ListQuizzPlayer)
            {
                if (p.UserName == userName)
                {
                    return true;
                }
            }
            return false;
        }

    }

    public class Player
    {
        public string UserName { get; set; }
        public int Score;

        public Player(string userName, int score)
        {
            this.UserName = userName;
            this.Score = 0;
        }

    }

}

