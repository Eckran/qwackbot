﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QwackBot.Models;

namespace QwackBot.Repository
{
    public class Recipe
    {
        private readonly BotDbContext _context;

        public Recipe(BotDbContext context)
        {
            _context = context;
        }
        public string GetRandom()
        {
            return _context.Recipe.OrderBy(r => Config.random.Next()).First().recipe;
        }

        public int AddAll(List<string> duckRecipes)
        {
            _context.Recipe.AddRange(duckRecipes.Select(r => new Models.Recipe() { recipe = r }).ToList());
            return _context.SaveChanges();
        }
    }
}
