﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QwackBot.Models;

namespace QwackBot.Repository
{
    public class User
    {
        private readonly BotDbContext _context;

        public User(BotDbContext context)
        {
            _context = context;
        }

        public float GetPoints(ulong discordId)
        {
            return _context.User.Single(u => u.DiscordId == discordId).Points;
        }

        public float RemovePoint(ulong authorId)
        {
            return ChangePoint(authorId, -1).Points;
        }

        public float AddPoint(ulong authorId)
        {
            return ChangePoint(authorId, 0.05F).Points;
        }

        private Models.User ChangePoint(ulong authorId, float value)
        {
            Models.User user = _context.User.Single(u => u.DiscordId == authorId);
            user.Points += value;
            _context.SaveChanges();
            return user;
        }

        public int IncrementNbMessage(ulong authorId)
        {
            Models.User user = _context.User.Single(u => u.DiscordId == authorId);
            user.MessageNumber++;
            _context.SaveChanges();
            return user.MessageNumber;
        }

        public bool Exists(ulong authorId)
        {
            return Get(authorId) != null;
        }

        public Models.User Get(ulong authorId)
        {
            return _context.User.FirstOrDefault(u => u.DiscordId == authorId);
        }

        public Models.User Add(Models.User user)
        {
            _context.User.Add(user);
            _context.SaveChanges();
            return user;
        }
    }
}
