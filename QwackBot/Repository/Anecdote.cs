﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QwackBot.Models;

namespace QwackBot.Repository
{
    public class Anecdote
    {
        private readonly BotDbContext _context;

        public Anecdote(BotDbContext context)
        {
            _context = context;
        }
        public string GetRandom()
        {
            return _context.Anecdote.OrderBy(r => Config.random.Next()).First().anecdote;
        }

        public int AddAll(List<string> anecdotes)
        {
            _context.Anecdote.AddRange(anecdotes.Select(a => new Models.Anecdote() { anecdote = a }).ToList());
            return _context.SaveChanges();
        }
    }
}
