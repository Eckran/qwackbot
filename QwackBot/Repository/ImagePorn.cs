﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QwackBot.Models;

namespace QwackBot.Repository
{
    public class ImagePorn
    {
        private readonly BotDbContext _context;

        public ImagePorn(BotDbContext context)
        {
            _context = context;
        }
        public string GetRandom()
        {
            return _context.ImagePorn.OrderBy(r => Config.random.Next()).First().uri;
        }

        public int AddAll(List<string> imagePorn)
        {
            _context.ImagePorn.AddRange(imagePorn.Select(u => new Models.ImagePorn() { uri = u }).ToList());
            return _context.SaveChanges();
        }
    }
}
