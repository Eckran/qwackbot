﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QwackBot.Models;

namespace QwackBot.Repository
{
    public class DuckJoke
    {
        private readonly BotDbContext _context;

        public DuckJoke(BotDbContext context)
        {
            _context = context;
        }
        public string GetRandom()
        {
            return _context.DuckJoke.OrderBy(r => Config.random.Next()).First().joke;
        }

        public int AddAll(List<string> duckJokes)
        {
            _context.DuckJoke.AddRange(duckJokes.Select(j => new Models.DuckJoke() { joke = j }).ToList());
            return _context.SaveChanges();
        }
    }
}
