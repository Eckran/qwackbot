﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using QwackBot.Models;

namespace QwackBot.Repository
{
    public class BadWord
    {
        private readonly BotDbContext _context;

        public BadWord(BotDbContext context)
        {
            _context = context;
        }
        public bool ContainsBadWord(string content)
        {
            return _context.BadWord.FirstOrDefault(b => content.Contains(b.insult)) != null;
        }

        public int AddAll(List<string> badWords)
        {            
            _context.BadWord.AddRange(badWords.Select(b => new Models.BadWord() { insult = b }).ToList());
            return _context.SaveChanges();
        }

        public int Count()
        {
            return _context.BadWord.Count();            
        }
    }
}
